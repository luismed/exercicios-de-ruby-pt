#objetos sao variaveis das classes
#Abstração = isola objetos q vc quer representar
#Encapsulamento = algo como uma funcao/metedo
# Função = Metodo / Variaveis = Atributos
class Animal
	@@animais_criados = 0 #atributo uma variavel criado dentro de uma classe
	def initialize #Metodo uma função dentrod e uma classe
		@especie = "Cão" #atributo da função
		@@animais_criados += 1 #atributo da classe
	end
end

class Mamifero
	def respira
		puts "inspirando e respirando"
	end
end

class Cachorro < Mamifero #classe Cachorro herda metodo da classe Mamifero
	def late
		puts"Au Au"
	end
end

class Gato < Mamifero #classe Gato herda metodo da classe Mamifero
	def mia
		puts "Miau"
	end
end

dog = Cachorro.new
dog.respira
dog.late
cat = Gato.new
cat.respira
cat.mia