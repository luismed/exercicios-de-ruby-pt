class Comida
    @@nome
    @@caloria
    def initialize
        @@nome = ""
        @@caloria = 0  
    end
    def Adicionar(nome,caloria)
        @@nome = nome
        @@caloria = caloria
    end
    def Mostrar
        puts "#{@@nome} tem #{@@caloria} calorias"
    end
    def Saudavel
        if @@caloria < 200
            puts "é bem saudavel"
        else
            puts "não é muito saudavel"
        end
    end
end

nome = gets.chomp
calorias = gets.chomp.to_i
comida = Comida.new
comida.Adicionar(nome,calorias)
comida.Mostrar
comida.Saudavel